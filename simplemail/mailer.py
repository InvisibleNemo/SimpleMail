import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders
import os

def sendMail(to, cc, fro, subject, text, html, files=[],server="localsmtp.invisiblenemo.com"):
    assert type(to)==list
    assert type(files)==list

    print(files)
    msg = MIMEMultipart()
    msg['From'] = fro
    msg['To'] = ', '.join(to)
    msg['cc'] = ', '.join(cc)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    print(msg)

    msg.attach( MIMEText(text, 'text'))
    msg.attach( MIMEText(html, 'html'))

    for file in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(file,"rb").read() )
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"'
                       % os.path.basename(file))
        msg.attach(part)

    toaddr = to + cc

    smtp = smtplib.SMTP(server)
    smtp.sendmail(fro, toaddr, msg.as_string() )
    smtp.close()

# Example:
# 
# sendMail(['Email Support <email@invisiblenemo.com>'],'INEMO Admin <masnun@leevio.com>','Hello Python!','Heya buddy! Say hello to Python! :)',['foo.py','bar.php'])

# Parts of this code has been taken from https://realpython.com/python-send-email/