import os
import simplemail
from datetime import datetime

today = datetime.today()

DayOfWeek = datetime(today.year, today.month, 5).strftime("%A")
month = datetime(today.year, today.month,1).strftime("%B")
ts_month = datetime(today.year, today.month-1,1).strftime("%B")
ts_month_numeric = datetime(today.year, today.month-1,1).strftime("%m") 
day = '3' # Choose the day of the month this should go out
year = today.strftime("%Y")

def reminder(mail_content_path, mail_list_path, text_body='', attachments=[]):

    mail_content_f = open(mail_content_path,'r')
    mail_contents = mail_content_f.read()
    mail_contents = mail_contents.replace("{{DayOfWeek}}",DayOfWeek)
    mail_contents = mail_contents.replace("{{month}}",month)
    mail_contents = mail_contents.replace("{{ts_month}}",ts_month)
    mail_contents = mail_contents.replace("{{day}}",day)
    mail_contents = mail_contents.replace("{{year}}",year) 
    mail_list_f = open(mail_list_path,'r')
    mail_list = eval(mail_list_f.read())
    print(mail_list['cc'])
    simplemail.sendMail(mail_list['to'],mail_list['cc'] ,'noreply-inemo@invisiblenemo.com','Job Reminder',text_body,mail_contents,attachments)

reminder('job_reminder_current_month_template.html','mailer_list.txt','',['attachments/imposter_syndrome.png'])
